package part1;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;


public class DeterministicAutomationReal implements DeterministicAutomaton{

	//    Q is a finite non empty set of states,
	private State [] state;
	//    V is a finite non empty set of inputs,
	private String [] input;
	//    T is a subset of QxVxQ, called set of transitions,
	private Transition [] transition;
	//    I is a non empty subset of Q, called set of initial states,
	private State initState;
	//    F is a subset of Q called set of final states.
	private State [] finalState;

	public DeterministicAutomationReal(State [] stat, String [] inpu, Transition [] tran, State init, State [] fina){
		state = stat;
		input = inpu;
		transition = tran;
		initState = init;
		finalState = fina;
	}
	//returns the initial state of the automaton
	@Override
	public State initialState() {
		return initState;
	}
	//returns the transition of source s and label label if it exists, null otherwise.
	//If the state s does not belong to the automaton, the method will thrown a java.lang.NoSuchElementException.
	@Override
	public Transition transition(State s, Object label) {
		if (Arrays.asList(state).contains(s)){
			for(Transition trans : transition){
				if (trans.getSource().getString().equals(s.getString())){
					if (trans.getLabel().equals(label)){
						return trans;
					}
				}
			}
			return null;
		}
		else{
			throw new NoSuchElementException();
		}
	}

	@Override
	public boolean recognize(Object[] word) {
		List<Object> wordd =  Arrays.asList(word);
		return recognize(wordd);
	}

	@Override
	public boolean recognize(List<Object> word) {
		Iterator itr = word.iterator();
		String letter = (String) itr.next();
		State nextState;
		//if (letter.equals(initialState().getString()))return false;
		
		if (this.transition(initState, letter).equals(null)) {
			System.out.println("This word is not accepted");
			return false;
		}
		else nextState = this.transition(initState, letter).getDest();
		
		while (itr.hasNext() ){
			letter = (String) itr.next();
			if (this.transition(nextState, letter).equals(null)) {
				System.out.println("This word is not accepted");
				return false;
			}
			else nextState = this.transition(nextState, letter).getDest();
		}
		
		if (Arrays.asList(finalState).contains(nextState)){
			System.out.println("This word is accepted");
			return true;
		}
		else {
			System.out.println("This word is not accepted");
			return false;
		}
	}

}
