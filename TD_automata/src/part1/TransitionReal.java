package part1;

public class TransitionReal implements Transition{
	private State source;
	private State destination;
	private String label;

	public TransitionReal(State sou, String lab, State des){
		source = sou;
		destination = des;
		label = lab;
	}
	@Override
	public State getSource() {
		return source;
	}
	@Override
	public State getDest() {
		return destination;
	}
	@Override
	public String getLabel() {
		return label;
	}
	
}
