package part1;

public interface Transition {
	public State getSource();
	public State getDest();
	public String getLabel();
}
