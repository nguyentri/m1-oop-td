package part1;
import java.util.ArrayList;
import java.util.Iterator;


public class test {
	public static void main(String[] args) {
		StateReal s1 = new StateReal("1");
		StateReal s2 = new StateReal("2");
		StateReal s3 = new StateReal("3");
		String a = "a";
		String b = "b";
		TransitionReal t1 = new TransitionReal(s1, a, s1);
		TransitionReal t2 = new TransitionReal(s1, b, s2);
		TransitionReal t3 = new TransitionReal(s2, a, s3);
		TransitionReal t4 = new TransitionReal(s3, a, s2);
		TransitionReal t5 = new TransitionReal(s3, b, s1);
		
		StateReal [] stat = {s1,s2,s3};
		String[] inpu = {a,b};
		Transition[] tran = {t1,t2,t3,t4,t5};
		StateReal init = s1;
		StateReal[] fina = {s1,s2};
		
		DeterministicAutomationReal automata = new DeterministicAutomationReal(stat, inpu, tran, init, fina);
		
		String [] test = new String [] {new String("a"),new String("b"),new String("a")};
		String [] test2 = new String [] {new String("a"),new String("b"),new String("a"),new String("b")};
		automata.recognize(test);
		automata.recognize(test2);
	}

}
