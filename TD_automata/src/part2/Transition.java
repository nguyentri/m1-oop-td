package part2;

public interface Transition {
	public State getSource();
	public State getDest();
	public String getLabel();
}
