package part2;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public interface DeterministicAutomaton {
	public State initialState();
	public Transition transition(State s, Object label);
	public boolean recognize(Object [] word);
	public boolean recognize(List<Object> word);
}
