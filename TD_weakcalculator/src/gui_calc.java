/*1 Give the tree of the graphic components.
 * Frame -> Panel -> button,label,button
 *2 Give the layout(s) used in this interface.
 * I use FlowLayout for this interface
*/
import java.awt.*;
import java.awt.event.*;
import java.lang.Integer;

public class gui_calc extends Frame implements ActionListener{ 

	private Panel panel;
	private TextField op1;
	private Label pl;
	private TextField op2;
	private Button plus;
	private TextField res;


	public gui_calc(){
		setLayout(new FlowLayout(2));
		
		panel = new Panel();
		op1 = new TextField();
		pl = new Label("+");
		op2 = new TextField();
		plus = new Button("=");
		res = new TextField();
		
		op1.setPreferredSize(new Dimension(50, 25));
		op2.setPreferredSize(new Dimension(50,25));
		res.setPreferredSize(new Dimension(50,25));
		plus.setPreferredSize(new Dimension(50,25));
		res.setEditable(false);
		plus.addActionListener(this);
		
		panel.add(op1);
		panel.add(pl);
		panel.add(op2);
		panel.add(plus);
		panel.add(res);
		
		add(panel);
		
		setTitle("Weak Calculator");
		setSize(260,100);
		setVisible(true);
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		int sum = Integer.parseInt(op1.getText()) + Integer.parseInt(op2.getText());
		res.setText(Integer.toString(sum));
	}

}
