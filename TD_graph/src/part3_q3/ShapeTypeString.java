package part3_q3;

public class ShapeTypeString {
	public String med_circle = "medium_circle";
	public int med_circle_diameter = 50;
	public String med_square = "medium_square";
	public int med_square_edge = 50;
	public String joint_point = "joint_point";
	public int joint_point_diameter = 30;
}
