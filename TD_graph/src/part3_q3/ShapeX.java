package part3_q3;

import java.awt.Dimension;
import java.awt.Point;

public abstract class ShapeX implements Cloneable{
	protected String _shapeType;
	protected Point _shapeCoor;
	protected Dimension _shapeWidthHeight;
	
	public String getShapeType(){
		return _shapeType;
	}
	public Point getShapeCoor(){
		return _shapeCoor;
	}
	public Dimension getShapeWidthHeight(){
		return _shapeWidthHeight;
	}
	
	public void setShapeType(String a){
		_shapeType = a;
	}
	public void setShapeCoor(Point a){
		_shapeCoor = a;
	}
	public void setShapeWidthHeight(Dimension a){
		_shapeWidthHeight = a;
	}
	
	public Object clone(){
		Object clone = null;
		try{
			clone = super.clone();
		}
		catch(CloneNotSupportedException e){
			e.printStackTrace();
		}
		return clone;
	}
}
