package part3_q3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.WindowConstants;
import javax.swing.border.Border;


public class GraphFrame extends JFrame{
	private GraphComponent _graphComponent;
	
	public GraphFrame(GraphComponent _graphCom){
		
		_graphComponent = _graphCom;
		_graphComponent.setBorder(BorderFactory.createLineBorder(Color.yellow));
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setTitle("TD_GRAPHS");
		
		JScrollPane scrollPane = new JScrollPane(_graphComponent);
		
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		JMenuItem menuItemOpen = new JMenuItem("Open new window");
		JMenuItem menuItemClose = new JMenuItem("Close window");
		JMenuItem menuItemQuit = new JMenuItem("Quit program");
		menu.add(menuItemOpen);menu.add(menuItemClose);menu.add(menuItemQuit);menuBar.add(menu);
		this.setJMenuBar(menuBar);
		
		JToolBar leftToolBar = new JToolBar();
		leftToolBar.setRollover(true);
		leftToolBar.setLayout(new GridLayout(2,1));
		
		JButton buttonRec = new JButton("Rectangle");
	    leftToolBar.add(buttonRec);
	    JButton buttonCir = new JButton("Circle");
	    leftToolBar.add(buttonCir);

		Container c = getContentPane();
		c.add(leftToolBar, BorderLayout.WEST);
		c.add(scrollPane,BorderLayout.CENTER);
		c.setPreferredSize(new Dimension(600, 500));

		menuItemOpen.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new GraphFrame(new GraphComponent());
			}
		});
		
		menuItemClose.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int choice = JOptionPane.showOptionDialog(null, "Are you really want to quit?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"yes","no"},"yes");
				setVisible(false);
				dispose();
			}
		});
		
		menuItemQuit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int choice = JOptionPane.showOptionDialog(null, "Are you really want to quit?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"yes","no"},"yes");
				if(choice == 0) System.exit(0);
			}
		});
		
		buttonRec.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				_graphComponent.setWhichShape("medium_square");
			}
		});
		
		buttonCir.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				_graphComponent.setWhichShape("medium_circle");
			}
		});
		setVisible(true);
		pack();
	}
}
