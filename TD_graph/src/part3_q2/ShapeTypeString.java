package part3_q2;

public class ShapeTypeString {
	public String med_circle = "medium_circle";
	public int med_circle_diameter = 50;
	public String med_square = "medium_square";
	public int med_square_edge = 50;
}
