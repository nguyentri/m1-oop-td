package part3_q2;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;



public class GraphComponent extends JComponent implements  MouseInputListener, KeyListener{
	private int radius=25;
	private List<Shape> _listCircle;
	private List<Shape> _listCircleDrag;
	private List<Shape> _listLine;
	private List<Shape> _listLineDrag;
	private int _currentX;
	private int _currentY;
	private int _initX;
	private int _initY;
	private boolean _isMouseInWindow;
	private boolean _isDragMouse = false; //check if last action is a drag
	private boolean _isAltDown = false;
	private String _whichShape = new ShapeTypeString().med_circle;
	private Shape _initShape = null;

	public GraphComponent(){
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		addKeyListener(this);
		_listCircle = new ArrayList<Shape>();
		_listCircleDrag = new ArrayList<Shape>();
		_listLine = new ArrayList<Shape>();
		_listLineDrag = new ArrayList<Shape>();

		this.setPreferredSize(new Dimension (900,900));
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		requestFocus();
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(new Color(100,0,190));

		if (!_listCircle.isEmpty())
			for (Shape circle : _listCircle){
				g2d.draw(circle);
			}
		if (!_listCircleDrag.isEmpty())
			for (Shape circle : _listCircleDrag){
				g2d.draw(circle);
			}
		if (!_listLine.isEmpty())
			for (Shape line : _listLine){
				g2d.draw(line);
			}
		if (!_listLineDrag.isEmpty())
			for (Shape line : _listLineDrag){
				g2d.draw(line);
			}

	}

	public void setWhichShape(String str){
		if (str.equals(new ShapeTypeString().med_square)) _whichShape = str;
		else _whichShape = new ShapeTypeString().med_circle;
	}

	//check mouse position inside or outside the circle, return true if inside
	private boolean insideOut(int x, int y){
		for(Shape circle : _listCircle){
			if(circle.contains(new Point(x,y))) return true;
		}
		return false;
	}


	//add new shape to the arraylist
	private void addShapeToArrayList(List<Shape> _listCircle2){
		System.out.println(_whichShape);
		if (_whichShape.equals(new ShapeTypeString().med_circle)) {
			Ellipse2D shape = new Ellipse2D.Float();
			shape.setFrame(_currentX-25, _currentY-25, new ShapeTypeString().med_circle_diameter, new ShapeTypeString().med_circle_diameter);
			_listCircle2.add(shape);
		}
		else if (_whichShape.equals(new ShapeTypeString().med_square)) {
			Rectangle2D shape = new Rectangle2D.Float();
			shape.setFrame(_currentX-25, _currentY-25, new ShapeTypeString().med_square_edge, new ShapeTypeString().med_square_edge);
			_listCircle2.add(shape);
		}
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (_isMouseInWindow){
			_currentX = (int)this.getMousePosition().getX();
			_currentY = (int)this.getMousePosition().getY();
		}
		if (!insideOut(_currentX, _currentY)) {
			addShapeToArrayList(_listCircle);
		}
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		_isMouseInWindow = true;
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		_isMouseInWindow = false;
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		//		_initShape = null;
		if (_isMouseInWindow && _initShape==null ){
			_initX = (int)this.getMousePosition().getX();
			_initY = (int)this.getMousePosition().getY();
		}else if(!(_initShape==null)){
			_initX = (int) _initShape.getBounds2D().getCenterX();
			_initY = (int) _initShape.getBounds2D().getCenterY();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		boolean isShapeHere = insideOut(_currentX, _currentY);
		Shape destShape = shapeUnderMouse();
		if (_isDragMouse) {
			_listCircleDrag.clear();
			addShapeToArrayList(_listCircle);

			if(isShapeHere) {
				System.out.println("cccc");
				_listCircle.remove(_listCircle.size()-1);
			}
		}
		if(_isAltDown && _initShape!=null){
			_listLine.add(new Line2D.Double(_initX,_initY,_currentX,_currentY));
			addShapeToArrayList(_listCircle);
			if(isShapeHere) {
				System.out.println("cccc");
				_listCircle.remove(_listCircle.size()-1);
				_listLine.remove(_listLine.size()-1);
				try {
					_listLine.add(new Line2D.Double(_initX,_initY,destShape.getBounds2D().getCenterX(),destShape.getBounds2D().getCenterY()));
				} catch (Exception e) {
					System.out.println("aloha");
				}
			}
		}
		_isDragMouse = false;
		_isAltDown = false;
		_initX = -1;
		_initY = -1;
		repaint();
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (_isMouseInWindow){
			_currentX = (int)this.getMousePosition().getX();
			_currentY = (int)this.getMousePosition().getY();
		}
		if (!_listCircleDrag.isEmpty() && _isDragMouse)_listCircleDrag.clear();
		if(!insideOut(_initX, _initY)) {
			addShapeToArrayList(_listCircleDrag);
			_isDragMouse = true;
		}
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
	}
	public Shape shapeUnderMouse(){
		for(Shape circle : _listCircle){
			if(circle.contains(new Point((int)this.getMousePosition().getX(),(int)this.getMousePosition().getY()))) return circle;
		}
		return null;
	}
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.isAltDown()) {
			_isAltDown = true;
			if(_initShape==null ){
				_initShape = shapeUnderMouse();
				System.out.println("shape null");
			}
			if (!(_initShape==null) && _initX != -1){
				_listLineDrag.clear();
				_listLineDrag.add(new Line2D.Double(_initX,_initY,_currentX,_currentY));
				repaint();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(!e.isAltDown()) _isAltDown = false;
		_initShape = null;
		_listLineDrag.clear();
		repaint();
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}



}
