package part2_q1;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.border.Border;


public class GraphFrame extends JFrame{
	private GraphComponent _graphComponent;
	
	public GraphFrame(GraphComponent _graphCom){
		
		_graphComponent = _graphCom;
		_graphComponent.setBorder(BorderFactory.createLineBorder(Color.yellow));
		
		Container c = getContentPane();
		c.add(_graphComponent);
		c.setPreferredSize(new Dimension(500, 500));

		setVisible(true);
		pack();
	}
}
