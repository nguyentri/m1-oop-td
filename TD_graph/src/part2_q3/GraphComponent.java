package part2_q3;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;


public class GraphComponent extends JComponent implements MouseInputListener{
	private int radius=25;
	private List<Point> listCircle;
	private int _currentX;
	private int _currentY;
	private int _initX;
	private int _initY;
	private boolean _isMouseInWindow;
	private boolean _isDragMouse = false; //check if last action is a drag

	public GraphComponent(){
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		listCircle = new ArrayList<Point>();
		this.setPreferredSize(new Dimension (900,900));
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(new Color(100,0,190));

		if (!listCircle.isEmpty())
			for (Point circle : listCircle){
				Ellipse2D shape = new Ellipse2D.Float();
				shape.setFrame(circle.x-25, circle.y-25, radius*2, radius*2);
				g2d.draw(shape);
			}
				
	}

	//check mouse position inside or outside the circle
	private boolean insideOut(int x, int y){
		for(Point circle : listCircle){
			if (((x-circle.x)*(x-circle.x) + (y-circle.y)*(y-circle.y)) < radius*radius) return true;
		}
		return false;
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {

		//		System.out.println(_currentX);
		//		System.out.println(_currentY);
		if (_isMouseInWindow){
			_currentX = (int)this.getMousePosition().getX();
			_currentY = (int)this.getMousePosition().getY();
		}
		if (!insideOut(_initX, _initY)) {
			//			System.out.println("cc");
			listCircle.add(new Point(_currentX, _currentY));
		}
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		_isMouseInWindow = true;
//		System.out.println("in");
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		_isMouseInWindow = false;
//		System.out.println("out");
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (_isMouseInWindow){
			_initX = (int)this.getMousePosition().getX();
			_initY = (int)this.getMousePosition().getY();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if (_isDragMouse) {
			listCircle.add(new Point(_currentX, _currentY));
		}
		repaint();
		_isDragMouse = false;
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (_isMouseInWindow){
			_currentX = (int)this.getMousePosition().getX();
			_currentY = (int)this.getMousePosition().getY();
		}

		if (!listCircle.isEmpty() && _isDragMouse)listCircle.remove(listCircle.size()-1);
		if(!insideOut(_initX, _initY)) {
			listCircle.add(new Point(_currentX, _currentY));
			_isDragMouse = true;
		}
		repaint();
		//		System.out.println(listCircle.size());	
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {

	}

}
