package part2_q3;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.border.Border;


public class GraphFrame extends JFrame{
	private GraphComponent _graphComponent;
	
	public GraphFrame(GraphComponent _graphCom){
		
		_graphComponent = _graphCom;
		_graphComponent.setBorder(BorderFactory.createLineBorder(Color.yellow));
		
		JScrollPane scrollPane = new JScrollPane(_graphComponent);
		
		Container c = getContentPane();
		c.add(scrollPane);
		c.setPreferredSize(new Dimension(500, 500));

		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		JMenuItem menuItemOpen = new JMenuItem("Open new window");
		JMenuItem menuItemClose = new JMenuItem("Close window");
		JMenuItem menuItemQuit = new JMenuItem("Quit program");
		menu.add(menuItemOpen);menu.add(menuItemClose);menu.add(menuItemQuit);menuBar.add(menu);
		this.setJMenuBar(menuBar);
		
		menuItemOpen.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new GraphFrame(new GraphComponent());
			}
		});
		
		menuItemClose.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int choice = JOptionPane.showOptionDialog(null, "Are you really want to quit?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"yes","no"},"yes");
				setVisible(false);
				dispose();
			}
		});
		
		menuItemQuit.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int choice = JOptionPane.showOptionDialog(null, "Are you really want to quit?", "Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, new String[]{"yes","no"},"yes");
				if(choice == 0) System.exit(0);
			}
		});
		
		

		setVisible(true);
		pack();
	}
}
