package part1_q3;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;


public class GraphComponent extends JComponent implements MouseInputListener{
	private int radius=25;
	
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		Graphics2D g2d = (Graphics2D) g;
		
		g2d.setColor(new Color(100,0,190));

		g2d.fillOval(200, 200, radius*2, radius*2);
	}
	
	//check mouse position inside or outside the circle
	private boolean insideOut(int x, int y){
		if (((x-200-radius)*(x-200-radius) + (y-200-radius)*(y-200-radius)) < radius*radius) return true;
		else return false;
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {
		if (insideOut((int)this.getMousePosition().getX(), (int)this.getMousePosition().getY())) System.out.println("WIN!");
		else System.out.println("LOSE!");
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {
		
	}

}
