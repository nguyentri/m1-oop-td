package part3_q1;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Double;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;



public class GraphComponent extends JComponent implements  MouseInputListener {
	private int radius=25;
	private List<Shape> _listCircle;
	private List<Shape> _listCircleDrag;
	private int _currentX;
	private int _currentY;
	private int _initX;
	private int _initY;
	private boolean _isMouseInWindow;
	private boolean _isDragMouse = false; //check if last action is a drag
	private String _whichShape = new ShapeTypeString().med_circle;

	public GraphComponent(){
		this.addMouseMotionListener(this);
		this.addMouseListener(this);
		_listCircle = new ArrayList<Shape>();
		_listCircleDrag = new ArrayList<Shape>();
		this.setPreferredSize(new Dimension (900,900));
	}

	public void paintComponent(Graphics g){
		super.paintComponent(g);
		requestFocus();
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(new Color(100,0,190));

		if (!_listCircle.isEmpty())
			for (Shape circle : _listCircle){
				g2d.draw(circle);
				//				if (circle.getShapeType().equals(new ShapeTypeString().med_square)){
				//					Rectangle2D shape = new Rectangle2D.Float();
				//					shape.setFrame(circle.getShapeCoor().x-25, circle.getShapeCoor().y-25, circle.getShapeWidthHeight().height, circle.getShapeWidthHeight().width);
				//					g2d.draw(shape);
				//				}else if(circle.getShapeType().equals(new ShapeTypeString().med_circle)){
				//					Ellipse2D shape = new Ellipse2D.Float();
				//					shape.setFrame(circle.getShapeCoor().x-25, circle.getShapeCoor().y-25, circle.getShapeWidthHeight().height*2, circle.getShapeWidthHeight().width*2);
				//					g2d.draw(shape);
				//				}

			}
		if (!_listCircleDrag.isEmpty())
			for (Shape circle : _listCircleDrag){
				g2d.draw(circle);
				//				if (circle.getShapeType().equals(new ShapeTypeString().med_square)){
				//					Rectangle2D shape = new Rectangle2D.Float();
				//					shape.setFrame(circle.getShapeCoor().x-25, circle.getShapeCoor().y-25, circle.getShapeWidthHeight().height, circle.getShapeWidthHeight().width);
				//					g2d.draw(shape);
				//				}else if(circle.getShapeType().equals(new ShapeTypeString().med_circle)){
				//					Ellipse2D shape = new Ellipse2D.Float();
				//					shape.setFrame(circle.getShapeCoor().x-25, circle.getShapeCoor().y-25, circle.getShapeWidthHeight().height*2, circle.getShapeWidthHeight().width*2);
				//					g2d.draw(shape);
				//				}

			}
	}

	public void setWhichShape(String str){
		if (str.equals(new ShapeTypeString().med_square)) _whichShape = str;
		else _whichShape = new ShapeTypeString().med_circle;
	}

	//check mouse position inside or outside the circle, return true if inside
	private boolean insideOut(int x, int y){
		for(Shape circle : _listCircle){
			if(circle.contains(new Point(x,y))) return true;
			//			if(circle.getShapeType().equals(new ShapeTypeString().med_circle)){
			//				if (((x-circle.getShapeCoor().x)*(x-circle.getShapeCoor().x) + (y-circle.getShapeCoor().y)*(y-circle.getShapeCoor().y)) < circle.getShapeWidthHeight().height*circle.getShapeWidthHeight().width ) 
			//					return true;
			//			}else if(circle.getShapeType().equals(new ShapeTypeString().med_square)){
			//				if ((x-circle.getShapeCoor().x)*(x-circle.getShapeCoor().x) < circle.getShapeWidthHeight().width*circle.getShapeWidthHeight().width && (y-circle.getShapeCoor().y)*(y-circle.getShapeCoor().y) < circle.getShapeWidthHeight().height*circle.getShapeWidthHeight().height ) 
			//					return true;
			//			}


		}
		return false;
	}

	//add new shape to the arraylist
	private void addShapeToArrayList(List<Shape> _listCircle2){
		System.out.println(_whichShape);
		if (_whichShape.equals(new ShapeTypeString().med_circle)) {
			Ellipse2D shape = new Ellipse2D.Float();
			shape.setFrame(_currentX-25, _currentY-25, new ShapeTypeString().med_circle_diameter, new ShapeTypeString().med_circle_diameter);
			_listCircle2.add(shape);
		}
		else if (_whichShape.equals(new ShapeTypeString().med_square)) {
			Rectangle2D shape = new Rectangle2D.Float();
			shape.setFrame(_currentX-25, _currentY-25, new ShapeTypeString().med_square_edge, new ShapeTypeString().med_square_edge);
			_listCircle2.add(shape);
		}
	}
	@Override
	public void mouseClicked(MouseEvent arg0) {

		//		System.out.println(_currentX);
		//		System.out.println(_currentY);
		if (_isMouseInWindow){
			_currentX = (int)this.getMousePosition().getX();
			_currentY = (int)this.getMousePosition().getY();
		}
		if (!insideOut(_initX, _initY)) {
			//			System.out.println("cc");
			addShapeToArrayList(_listCircle);
		}
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		_isMouseInWindow = true;
		//		System.out.println("in");
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		_isMouseInWindow = false;
		//		System.out.println("out");
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (_isMouseInWindow){
			_initX = (int)this.getMousePosition().getX();
			_initY = (int)this.getMousePosition().getY();
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		boolean isShapeHere = insideOut(_currentX, _currentY);
		if (_isDragMouse) {
			_listCircleDrag.clear();
			addShapeToArrayList(_listCircle);

			if(isShapeHere) {
				System.out.println("cccc");
				_listCircle.remove(_listCircle.size()-1);
				repaint();
			}
		}
		_isDragMouse = false;

	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		if (_isMouseInWindow){
			_currentX = (int)this.getMousePosition().getX();
			_currentY = (int)this.getMousePosition().getY();
		}
		if (!_listCircleDrag.isEmpty() && _isDragMouse)_listCircleDrag.clear();
		if(!insideOut(_initX, _initY)) {
			addShapeToArrayList(_listCircleDrag);
			_isDragMouse = true;
		}
		repaint();
		//		System.out.println(listCircle.size());	
	}

	@Override
	public void mouseMoved(MouseEvent arg0) {

	}




}
