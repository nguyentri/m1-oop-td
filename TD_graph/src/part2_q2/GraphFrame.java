package part2_q2;
import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.border.Border;


public class GraphFrame extends JFrame{
	private GraphComponent _graphComponent;
	
	public GraphFrame(GraphComponent _graphCom){
		
		_graphComponent = _graphCom;
		_graphComponent.setBorder(BorderFactory.createLineBorder(Color.yellow));
		
		JScrollPane scrollPane = new JScrollPane(_graphComponent);
		
		Container c = getContentPane();
		c.add(scrollPane);
		c.setPreferredSize(new Dimension(500, 500));

		
		

		setVisible(true);
		pack();
	}
}
